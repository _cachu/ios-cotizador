﻿
using System;
using System.Linq;
using System.Collections.Generic;

using MonoTouch.Dialog;
using System.Xml.Linq;
using SQLite;

using Foundation;
using UIKit;
using System.Xml.Serialization;
using System.IO;

using Google.Apis;
using MapKit;
using CoreLocation;

namespace Examen2iOs
{
	public partial class dvcMenu : DialogViewController
	{


		void CreateDatabase ()
		{
			string dbPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "sqlitetest.db3");
			/*if (File.Exists (dbPath))
				return;*/
			var db = new SQLiteConnection (dbPath);
			db.DropTable<Coche> ();
			db.CreateTable<Coche> ();
			db.DropTable<Marca> ();
			db.CreateTable<Marca> ();
			db.Close ();
		}

		void getMarcas ()
		{
			Marca marca1 = new Marca ();
			XDocument marcas = XDocument.Load ("Marcas.xml");

			string dbpath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "sqlitetest.db3");

			var db = new SQLiteConnection (dbpath);
			try {
				foreach (XElement marca in marcas.Descendants ("marca")) {

					marca1 = new Marca () {
						idmarca = int.Parse (marca.Element ("idmarca").Value),
						nombre = marca.Element ("nombre").Value,
						tasa = double.Parse(marca.Element ("tasa").Value),
						logo=marca.Element("logo").Value
					};
					db.Insert (marca1, typeof(Marca));
				}
			} catch (Exception ex) {
				new UIAlertView ("error", ex.Message, null, "ok", null).Show ();
			} finally {
				db.Close ();
			}
		}

		void getCoches ()
		{
			CreateDatabase ();
			Coche coche = new Coche ();
			XDocument autos = XDocument.Load ("Coches.xml");

			string dbpath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "sqlitetest.db3");

			var db = new SQLiteConnection (dbpath);
			try {
				foreach (XElement auto in autos.Descendants ("auto")) {

					coche = new Coche () {
						idauto = int.Parse (auto.Element ("idauto").Value),
						nombre = auto.Element ("nombre").Value,
						costo = double.Parse (auto.Element ("costo").Value),
						foto = auto.Element ("foto").Value,
						idmarca = int.Parse (auto.Element ("idmarca").Value)
					};
					db.Insert (coche, typeof(Coche));
				}
			} catch (Exception ex) {
				new UIAlertView ("error", ex.Message, null, "ok", null).Show ();
			} finally {
				db.Close ();
			}
		}

		public dvcMenu () : base (UITableViewStyle.Grouped, null)
		{
			string dbPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "sqlitetest.db3");
			var db = new SQLiteConnection (dbPath);
			getCoches ();
			getMarcas ();
			Root = new RootElement ("dvcMenu");
			var section = new Section ();
			foreach (var marca in db.Table<Marca> ().ToList()) {
				section.Add (getSection (marca));

			}
			Root.Add (section);
		}
		MKMapView mapView;
		public dvcMenu (int id):base (UITableViewStyle.Grouped,null,true)
		{
			mapView = new MKMapView (View.Bounds);

			var segmento = new UISegmentedControl (UIScreen.MainScreen.Bounds);
			segmento = new UISegmentedControl (new CoreGraphics.CGRect(150,530,100,30));
			segmento.InsertSegment ("Segunda Sucursal",0,true);

			mapView.Delegate = new MapDelegate ();

			View.Add(mapView);
			View.AddSubview (segmento);
			var altura = MKMapCamera.CameraLookingAtCenterCoordinate (new CLLocationCoordinate2D (21.1528214, -101.7114419), new CLLocationCoordinate2D (21.1528214, -101.7114419), 15000);
			mapView.Camera = altura;

			segmento.ValueChanged += (sender, e) => {

				mapView = new MKMapView (View.Bounds);

				segmento = new UISegmentedControl (UIScreen.MainScreen.Bounds);
				segmento = new UISegmentedControl (new CoreGraphics.CGRect(150,530,100,30));
				//segmento.InsertSegment ("Segunda Sucursal",0,true);

				mapView.Delegate = new MapDelegate ();

				View.Add(mapView);
				View.AddSubview (segmento);
				altura = MKMapCamera.CameraLookingAtCenterCoordinate (new CLLocationCoordinate2D (21.1528214, -101.7114419), new CLLocationCoordinate2D (21.1528214, -101.7114419), 15000);
				mapView.Camera = altura;

				var ruta = (sender as UISegmentedControl).SelectedSegment;

				if(ruta==0){
					switch(id){
					case 1:

						trazarRuta (21.1479701,-101.6763928);
						break;
					case 2:

						trazarRuta (21.1470914,-101.7033436);
						break;
					case 3:

						trazarRuta (21.1426895,-101.7046687);
						break;
					}
				}
			};

			switch (id) {
			case 1:

				trazarRuta (21.1037294, -101.6467074);
				break;
			case 2:

				trazarRuta (21.1037256, -101.672972);
				break;
			case 3:

				trazarRuta (21.1495912,-101.6863342);
				break;
			}



		}
		public class MapDelegate:MKMapViewDelegate{
			public override MKOverlayRenderer OverlayRenderer (MKMapView mapView, IMKOverlay overlay)
			{
				if(overlay is MKPolyline){
					var route = (MKPolyline)overlay;
					var renderer = new MKPolylineRenderer (route){ StrokeColor = UIColor.FromRGB (255, 100, 100) };
					return renderer;
				}
				return null;
			}
		}
		void trazarRuta(double x, double y){
			var source = new MKMapItem (new MKPlacemark (new CLLocationCoordinate2D (21.1528214,-101.7114419),new NSDictionary()));
			var destination = new MKMapItem (new MKPlacemark (new CLLocationCoordinate2D (x,y),new NSDictionary()));

			var request = new MKDirectionsRequest {
				Source=source,
				Destination=destination,
				RequestsAlternateRoutes=false
			};

			var directions = new MKDirections (request);

			directions.CalculateDirections ((response, error) => {
				if(error!=null){
					Console.WriteLine(error.LocalizedDescription);
				}
				else{
					foreach(var route in response.Routes){
						mapView.AddOverlay(route.Polyline);
					}
				}
			});
		}

		ImageRootElement getSection (Marca marca)
		{


			var mapa = new Section () {
				new StringElement("Mapa",()=>{
					NavigationController.PushViewController(new dvcMenu(marca.idmarca),true);
				})
			};

			string dbPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "sqlitetest.db3");
			var db = new SQLiteConnection (dbPath);

			var img = UIImage.FromBundle("img/"+marca.logo);
			try {
				var root = new ImageRootElement (marca.nombre,img);
				var section = new Section ();
				foreach (var auto in db.Table<Coche> ().ToList().Where(n=>n.idmarca==marca.idmarca)) {
					section.Add (getSection2 (marca, auto));
				}
				root.Add(section);
				root.Add (mapa);

				return root;
			} catch (Exception) {
				return null;
			}
		}

		ImageRootElement getSection2 (Marca marca, Coche coche)
		{
			EntryElement mensualidad;
			EntryElement enganche;

			var valEnganche = (coche.costo * (0.15)).ToString ();
			var img = UIImage.FromBundle("img/"+coche.foto);
			var section = 
				new ImageRootElement (coche.nombre,img) {
					new Section () {
						(mensualidad = new EntryElement ("Mensualidad", "Ingrese mensualidad", "12")),
						(enganche = new EntryElement ("Enganche", "Ingrese enganche", valEnganche)),
						new StringElement ("Calcular", () => {
							calcular (marca, coche, mensualidad, enganche);
						})
					}
				};
			return section;
		}

		void calcular (Marca marca, Coche coche, EntryElement mensualidad, EntryElement enganche)
		{
			var anual = int.Parse (mensualidad.Value) / 12;
			var subtotal = coche.costo - double.Parse (enganche.Value);
			var intereses = subtotal * (marca.tasa / 100);
			var total = subtotal + (intereses * anual);
			var totalMes = total / int.Parse (mensualidad.Value);

			new UIAlertView ("Total","Precio Total:\n$"+coche.costo.ToString()+"\nMonto a financiar:\n$"+subtotal.ToString()+"\nIntereses:\n$"+intereses.ToString()+"\nMensualidad:\n$"+ Math.Round (totalMes, 2).ToString (), null, "Ok", null).Show ();
		}
	}

	public class Marca
	{
		public int idmarca {
			get;
			set;
		}

		public string nombre {
			get;
			set;
		}

		public double tasa {
			get;
			set;
		}
		public string logo {
			get;
			set;
		}
	}

	//[Serializable,XmlType (TypeName = "auto")]
	public class Coche
	{
		public int idauto {
			get;
			set;
		}

		public int idmarca {
			get;
			set;
		}

		public string nombre {
			get;
			set;
		}

		public double costo {
			get;
			set;
		}

		public string foto {
			get;
			set;
		}
	}
	public class ImageRootElement : RootElement
	{
		private UIImage _image;

		public override UIKit.UITableViewCell GetCell (UIKit.UITableView tv)
		{
			var baseCell = base.GetCell (tv); 
			var cell = new UITableViewCell (UITableViewCellStyle.Subtitle, "cellId");
			cell.TextLabel.Text = Caption;

			cell.Accessory = baseCell.Accessory;
			cell.ImageView.Image = _image;
			return cell;
		}

		public ImageRootElement (string caption, UIImage image) : base(caption)
		{
			_image = image;
		}   
	}
}
